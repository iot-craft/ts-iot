# ts-iot 🌊

![](https://img.shields.io/gitlab/pipeline-status/iot-craft/ts-iot?branch=beta&logo=gitlab&style=for-the-badge)
![](https://img.shields.io/gitlab/coverage/iot-craft/ts-iot/beta?logo=jest&style=for-the-badge)
