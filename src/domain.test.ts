import { deepStrictEqual as assert } from "assert";
import { bitwiseAND, average } from "./domain";

test("bitwise AND", () => {
  assert(bitwiseAND(["1.000", "1.000", "1.000"]), true);
  assert(bitwiseAND(["1.000", "1.000", "0.000"]), false);
});

test("average", () => {
  assert(average(["1.000", "2.0", "3.0"]), 2.0);
});
