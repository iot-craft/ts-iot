export function bitwiseAND(children: string[]): boolean {
  return children.every((c) => Number(c));
}
export function average(children: string[]): number {
  return (
    children.map((c) => Number(c)).reduce((a, b) => a + b) / children.length
  );
}
