FROM node:16.18.1-buster-slim
WORKDIR /app
COPY cfg.yml .
COPY tsconfig.json .
COPY src src
COPY package.json .
COPY yarn.lock .
RUN rm src/*.test.ts
RUN yarn install --production
#ARG PORT
#ARG ENV
#ENV ENV=$ENV
#EXPOSE $PORT
CMD ["yarn", "ts-node", "--transpile-only", "src/mqtt.ts" ]

